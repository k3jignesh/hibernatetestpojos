package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Study_Material_Tb {

	private int ssm_id;
	private String ssm_file;
	private String ssm_document_name;
	private String ssm_uploaded_at;
	
		private School_Class_Stream_Join_Tb ssm_class_id;
	
	
	public int getSsm_id() {
		return ssm_id;
	}
	public void setSsm_id(int ssm_id) {
		this.ssm_id = ssm_id;
	}
	public String getSsm_file() {
		return ssm_file;
	}
	public void setSsm_file(String ssm_file) {
		this.ssm_file = ssm_file;
	}
	public String getSsm_document_name() {
		return ssm_document_name;
	}
	public void setSsm_document_name(String ssm_document_name) {
		this.ssm_document_name = ssm_document_name;
	}
	public String getSsm_uploaded_at() {
		return ssm_uploaded_at;
	}
	public void setSsm_uploaded_at(String ssm_uploaded_at) {
		this.ssm_uploaded_at = ssm_uploaded_at;
	}
	public School_Class_Stream_Join_Tb getSsm_class_id() {
		return ssm_class_id;
	}
	public void setSsm_class_id(School_Class_Stream_Join_Tb ssm_class_id) {
		this.ssm_class_id = ssm_class_id;
	}
	
	
	
}
