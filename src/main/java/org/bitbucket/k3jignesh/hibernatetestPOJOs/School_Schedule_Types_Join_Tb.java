package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Schedule_Types_Join_Tb {
	
	private int sstj_id;
	private String sstj_title;
	private String sstj_file;
	private School_Class_Stream_Join_Tb sstj_class_id;
	private Schedule_Types_Tb sstj_type;
	
	
	public int getSstj_id() {
		return sstj_id;
	}
	public void setSstj_id(int sstj_id) {
		this.sstj_id = sstj_id;
	}
	public String getSstj_title() {
		return sstj_title;
	}
	public void setSstj_title(String sstj_title) {
		this.sstj_title = sstj_title;
	}
	public String getSstj_file() {
		return sstj_file;
	}
	public void setSstj_file(String sstj_file) {
		this.sstj_file = sstj_file;
	}
	public School_Class_Stream_Join_Tb getSstj_class_id() {
		return sstj_class_id;
	}
	public void setSstj_class_id(School_Class_Stream_Join_Tb sstj_class_id) {
		this.sstj_class_id = sstj_class_id;
	}
	public Schedule_Types_Tb getSstj_type() {
		return sstj_type;
	}
	public void setSstj_type(Schedule_Types_Tb sstj_type) {
		this.sstj_type = sstj_type;
	}
	

}
