package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class Lab_Types_Tb {

	private int lt_id;
	private String lt_name;
	
	
	public int getLt_id() {
		return lt_id;
	}
	public void setLt_id(int lt_id) {
		this.lt_id = lt_id;
	}
	public String getLt_name() {
		return lt_name;
	}
	public void setLt_name(String lt_name) {
		this.lt_name = lt_name;
	}
	
	
	
}
