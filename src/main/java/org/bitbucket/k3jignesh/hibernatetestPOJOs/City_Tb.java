package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class City_Tb {

	private int c_id;
	private String c_name;
	
		private State_Tb c_state_id;
	
	
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public State_Tb getC_state_id() {
		return c_state_id;
	}
	public void setC_state_id(State_Tb c_state_id) {
		this.c_state_id = c_state_id;
	}
	
	
	
}
