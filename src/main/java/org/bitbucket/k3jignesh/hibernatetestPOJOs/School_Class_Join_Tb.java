package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Class_Join_Tb {
	private int scj_id;
	private School_Information_Tb scj_school_id;
	private Class_Tb scj_starting_class_id;
	private Class_Tb scj_ending_class_id;
	public int getScj_id() {
		return scj_id;
	}
	public void setScj_id(int scj_id) {
		this.scj_id = scj_id;
	}
	public School_Information_Tb getScj_school_id() {
		return scj_school_id;
	}
	public void setScj_school_id(School_Information_Tb scj_school_id) {
		this.scj_school_id = scj_school_id;
	}
	public Class_Tb getScj_starting_class_id() {
		return scj_starting_class_id;
	}
	public void setScj_starting_class_id(Class_Tb scj_starting_class_id) {
		this.scj_starting_class_id = scj_starting_class_id;
	}
	public Class_Tb getScj_ending_class_id() {
		return scj_ending_class_id;
	}
	public void setScj_ending_class_id(Class_Tb scj_ending_class_id) {
		this.scj_ending_class_id = scj_ending_class_id;
	}
	
}
