package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Faculty_Tb {

	private int sf_id;
	private String sf_name;
	private int sf_admission_privilege;
	private int sf_student_register_privilege;
	private int sf_school_detail_modify_privilege;
	
		private School_Information_Tb sf_school_id;
		private School_User_Tb sf_user_id;
		
		
		public int getSf_id() {
			return sf_id;
		}
		public void setSf_id(int sf_id) {
			this.sf_id = sf_id;
		}
		public String getSf_name() {
			return sf_name;
		}
		public void setSf_name(String sf_name) {
			this.sf_name = sf_name;
		}
		public int getSf_admission_privilege() {
			return sf_admission_privilege;
		}
		public void setSf_admission_privilege(int sf_admission_privilege) {
			this.sf_admission_privilege = sf_admission_privilege;
		}
		public int getSf_student_register_privilege() {
			return sf_student_register_privilege;
		}
		public void setSf_student_register_privilege(int sf_student_register_privilege) {
			this.sf_student_register_privilege = sf_student_register_privilege;
		}
		public int getSf_school_detail_modify_privilege() {
			return sf_school_detail_modify_privilege;
		}
		public void setSf_school_detail_modify_privilege(int sf_school_detail_modify_privilege) {
			this.sf_school_detail_modify_privilege = sf_school_detail_modify_privilege;
		}
		public School_Information_Tb getSf_school_id() {
			return sf_school_id;
		}
		public void setSf_school_id(School_Information_Tb sf_school_id) {
			this.sf_school_id = sf_school_id;
		}
		public School_User_Tb getSf_user_id() {
			return sf_user_id;
		}
		public void setSf_user_id(School_User_Tb sf_user_id) {
			this.sf_user_id = sf_user_id;
		}
		
	
	
	
}
