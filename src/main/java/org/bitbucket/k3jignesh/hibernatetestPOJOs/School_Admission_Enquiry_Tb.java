package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Admission_Enquiry_Tb {
	
	private int sae_id;
	private String sae_student_name;
	private String sae_birthday;
	private int sae_gender;
	private int sae_last_passed_class;
	private int sae_last_class_score;
	private String sae_current_school;	
	private String sae_address;
	private String sae_father_name;
	private String sae_mother_name;
	private String sae_father_current_occupation;
	private String sae_mother_current_occupation;
	private int sae_father_contact_no;
	private int sae_mother_contact_no;
	private String sae_father_email;
	private String sae_mother_email;
	private String sae_date;
	private String sae_time;
	private int sae_viewed;
	private int sae_last_class_passed_year;
	
		private Disability_Tb sae_disability_type;
		private City_Tb sae_city_id;
		private School_Information_Tb sae_school_id;
		private School_Board_Tb sae_board;
		private Subject_Stream_Tb sae_stream;
		private School_Medium_Tb sae_medium;
		
		public int getSae_id() {
			return sae_id;
		}
		public void setSae_id(int sae_id) {
			this.sae_id = sae_id;
		}
		public String getSae_student_name() {
			return sae_student_name;
		}
		public void setSae_student_name(String sae_student_name) {
			this.sae_student_name = sae_student_name;
		}
		public String getSae_birthday() {
			return sae_birthday;
		}
		public void setSae_birthday(String sae_birthday) {
			this.sae_birthday = sae_birthday;
		}
		public int getSae_gender() {
			return sae_gender;
		}
		public void setSae_gender(int sae_gender) {
			this.sae_gender = sae_gender;
		}
		public int getSae_last_passed_class() {
			return sae_last_passed_class;
		}
		public void setSae_last_passed_class(int sae_last_passed_class) {
			this.sae_last_passed_class = sae_last_passed_class;
		}
		public int getSae_last_class_score() {
			return sae_last_class_score;
		}
		public void setSae_last_class_score(int sae_last_class_score) {
			this.sae_last_class_score = sae_last_class_score;
		}
		
		public String getSae_current_school() {
			return sae_current_school;
		}
		public void setSae_current_school(String sae_current_school) {
			this.sae_current_school = sae_current_school;
		}
		public String getSae_address() {
			return sae_address;
		}
		public void setSae_address(String sae_address) {
			this.sae_address = sae_address;
		}
		public String getSae_father_name() {
			return sae_father_name;
		}
		public void setSae_father_name(String sae_father_name) {
			this.sae_father_name = sae_father_name;
		}
		public String getSae_mother_name() {
			return sae_mother_name;
		}
		public void setSae_mother_name(String sae_mother_name) {
			this.sae_mother_name = sae_mother_name;
		}
		public String getSae_father_current_occupation() {
			return sae_father_current_occupation;
		}
		public void setSae_father_current_occupation(String sae_father_current_occupation) {
			this.sae_father_current_occupation = sae_father_current_occupation;
		}
		public String getSae_mother_current_occupation() {
			return sae_mother_current_occupation;
		}
		public void setSae_mother_current_occupation(String sae_mother_current_occupation) {
			this.sae_mother_current_occupation = sae_mother_current_occupation;
		}
		public int getSae_father_contact_no() {
			return sae_father_contact_no;
		}
		public void setSae_father_contact_no(int sae_father_contact_no) {
			this.sae_father_contact_no = sae_father_contact_no;
		}
		public int getSae_mother_contact_no() {
			return sae_mother_contact_no;
		}
		public void setSae_mother_contact_no(int sae_mother_contact_no) {
			this.sae_mother_contact_no = sae_mother_contact_no;
		}
		public String getSae_father_email() {
			return sae_father_email;
		}
		public void setSae_father_email(String sae_father_email) {
			this.sae_father_email = sae_father_email;
		}
		public String getSae_mother_email() {
			return sae_mother_email;
		}
		public void setSae_mother_email(String sae_mother_email) {
			this.sae_mother_email = sae_mother_email;
		}
		
		public String getSae_date() {
			return sae_date;
		}
		public void setSae_date(String sae_date) {
			this.sae_date = sae_date;
		}
		public String getSae_time() {
			return sae_time;
		}
		public void setSae_time(String sae_time) {
			this.sae_time = sae_time;
		}
		public int getSae_viewed() {
			return sae_viewed;
		}
		public void setSae_viewed(int sae_viewed) {
			this.sae_viewed = sae_viewed;
		}
		public int getSae_last_class_passed_year() {
			return sae_last_class_passed_year;
		}
		public void setSae_last_class_passed_year(int sae_last_class_passed_year) {
			this.sae_last_class_passed_year = sae_last_class_passed_year;
		}
		public Disability_Tb getSae_disability_type() {
			return sae_disability_type;
		}
		public void setSae_disability_type(Disability_Tb sae_disability_type) {
			this.sae_disability_type = sae_disability_type;
		}
		public City_Tb getSae_city_id() {
			return sae_city_id;
		}
		public void setSae_city_id(City_Tb sae_city_id) {
			this.sae_city_id = sae_city_id;
		}
		public School_Information_Tb getSae_school_id() {
			return sae_school_id;
		}
		public void setSae_school_id(School_Information_Tb sae_school_id) {
			this.sae_school_id = sae_school_id;
		}
		public School_Board_Tb getSae_board() {
			return sae_board;
		}
		public void setSae_board(School_Board_Tb sae_board) {
			this.sae_board = sae_board;
		}
		public Subject_Stream_Tb getSae_stream() {
			return sae_stream;
		}
		public void setSae_stream(Subject_Stream_Tb sae_stream) {
			this.sae_stream = sae_stream;
		}
		public School_Medium_Tb getSae_medium() {
			return sae_medium;
		}
		public void setSae_medium(School_Medium_Tb sae_medium) {
			this.sae_medium = sae_medium;
		}
		
		
	
	

}
