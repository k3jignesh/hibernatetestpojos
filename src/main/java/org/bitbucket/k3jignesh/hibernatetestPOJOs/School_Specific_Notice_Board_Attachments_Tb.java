package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Specific_Notice_Board_Attachments_Tb {

	private int ssnba_id;
	private String ssnba_file;

		private School_Specific_Notice_Board_Tb ssnba_ssnb_id;
	
	
	public int getSsnba_id() {
		return ssnba_id;
	}
	public void setSsnba_id(int ssnba_id) {
		this.ssnba_id = ssnba_id;
	}
	public String getSsnba_file() {
		return ssnba_file;
	}
	public void setSsnba_file(String ssnba_file) {
		this.ssnba_file = ssnba_file;
	}
	public School_Specific_Notice_Board_Tb getSsnba_ssnb_id() {
		return ssnba_ssnb_id;
	}
	public void setSsnba_ssnb_id(School_Specific_Notice_Board_Tb ssnba_ssnb_id) {
		this.ssnba_ssnb_id = ssnba_ssnb_id;
	}
	
	
}
