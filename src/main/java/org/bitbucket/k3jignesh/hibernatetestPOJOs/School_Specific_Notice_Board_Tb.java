package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Specific_Notice_Board_Tb {

	private int ssnb_id;
	private String ssnb_notice_entry;
	private int ssnb_have_attachments;
	private String ssnb_uploaded_at;
	
	private School_Class_Stream_Join_Tb ssnb_class_id;
	
	
	public int getSsnb_id() {
		return ssnb_id;
	}
	public void setSsnb_id(int ssnb_id) {
		this.ssnb_id = ssnb_id;
	}
	public String getSsnb_notice_entry() {
		return ssnb_notice_entry;
	}
	public void setSsnb_notice_entry(String ssnb_notice_entry) {
		this.ssnb_notice_entry = ssnb_notice_entry;
	}
	public int getSsnb_have_attachments() {
		return ssnb_have_attachments;
	}
	public void setSsnb_have_attachments(int ssnb_have_attachments) {
		this.ssnb_have_attachments = ssnb_have_attachments;
	}
	public String getSsnb_uploaded_at() {
		return ssnb_uploaded_at;
	}
	public void setSsnb_uploaded_at(String ssnb_uploaded_at) {
		this.ssnb_uploaded_at = ssnb_uploaded_at;
	}
	public School_Class_Stream_Join_Tb getSsnb_class_id() {
		return ssnb_class_id;
	}
	public void setSsnb_class_id(School_Class_Stream_Join_Tb ssnb_class_id) {
		this.ssnb_class_id = ssnb_class_id;
	}
	
	
	
}
