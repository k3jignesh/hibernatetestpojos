package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_User_Type_tb {

	private int sut_id;
	private String sut_name;
	
	
	public int getSut_id() {
		return sut_id;
	}
	public void setSut_id(int sut_id) {
		this.sut_id = sut_id;
	}
	public String getSut_name() {
		return sut_name;
	}
	public void setSut_name(String sut_name) {
		this.sut_name = sut_name;
	}
	
	
	
}
