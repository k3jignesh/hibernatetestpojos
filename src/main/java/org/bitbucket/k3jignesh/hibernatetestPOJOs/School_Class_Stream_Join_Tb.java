package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Class_Stream_Join_Tb {
	
	private int scsj_id;

	
		private School_Information_Tb scsj_school_id;
		private Subject_Stream_Tb scsj_stream_id;
	
	
	public int getScsj_id() {
		return scsj_id;
	}
	public void setScsj_id(int scsj_id) {
		this.scsj_id = scsj_id;
	}
	
	public School_Information_Tb getScsj_school_id() {
		return scsj_school_id;
	}
	public void setScsj_school_id(School_Information_Tb scsj_school_id) {
		this.scsj_school_id = scsj_school_id;
	}
	
	public Subject_Stream_Tb getScsj_stream_id() {
		return scsj_stream_id;
	}
	public void setScsj_stream_id(Subject_Stream_Tb scsj_stream_id) {
		this.scsj_stream_id = scsj_stream_id;
	}
	
	

}
