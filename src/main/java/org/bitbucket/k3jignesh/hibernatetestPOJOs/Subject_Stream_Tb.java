package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class Subject_Stream_Tb {
	
	private int subs_id;
	private String subs_name;
	
	
	public int getSubs_id() {
		return subs_id;
	}
	public void setSubs_id(int subs_id) {
		this.subs_id = subs_id;
	}
	public String getSubs_name() {
		return subs_name;
	}
	public void setSubs_name(String subs_name) {
		this.subs_name = subs_name;
	}
	

}
