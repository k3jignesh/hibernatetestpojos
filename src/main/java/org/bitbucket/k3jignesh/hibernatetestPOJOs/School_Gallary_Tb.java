package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Gallary_Tb {

	private int sg_id;
	private String sg_image;
	private String sg_image_title;
	private String sg_image_description;
	
		private School_Information_Tb sg_school_id;
	
	
	public int getSg_id() {
		return sg_id;
	}
	public void setSg_id(int sg_id) {
		this.sg_id = sg_id;
	}
	public String getSg_image() {
		return sg_image;
	}
	public void setSg_image(String sg_image) {
		this.sg_image = sg_image;
	}
	public String getSg_image_title() {
		return sg_image_title;
	}
	public void setSg_image_title(String sg_image_title) {
		this.sg_image_title = sg_image_title;
	}
	public String getSg_image_description() {
		return sg_image_description;
	}
	public void setSg_image_description(String sg_image_description) {
		this.sg_image_description = sg_image_description;
	}
	public School_Information_Tb getSg_school_id() {
		return sg_school_id;
	}
	public void setSg_school_id(School_Information_Tb sg_school_id) {
		this.sg_school_id = sg_school_id;
	}
	
	
	
}
