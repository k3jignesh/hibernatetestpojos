package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class Main_Admin_Tb {

	private int ma_id;
	private String ma_name;
	private String ma_username;
	private String ma_password;

	
	public int getMa_id() {
		return ma_id;
	}
	public void setMa_id(int ma_id) {
		this.ma_id = ma_id;
	}
	public String getMa_name() {
		return ma_name;
	}
	public void setMa_name(String ma_name) {
		this.ma_name = ma_name;
	}
	public String getMa_username() {
		return ma_username;
	}
	public void setMa_username(String ma_username) {
		this.ma_username = ma_username;
	}
	public String getMa_password() {
		return ma_password;
	}
	public void setMa_password(String ma_password) {
		this.ma_password = ma_password;
	}
	
	
}
