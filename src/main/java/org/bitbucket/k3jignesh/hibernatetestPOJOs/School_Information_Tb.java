package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Information_Tb {

	private int si_id;
	private String si_name;
	private String si_logo;
	private String si_description;
	private String si_mission;
	private String si_address;
	private int si_boys_hostel;
	private int si_girls_hostel;
	private long si_primary_phone;
	private long si_secondary_phone;
	private String si_email;
	private long si_fax;
	private String si_website;
	private int si_is_active;
	
		private City_Tb si_city;
		private School_Board_Tb si_board;
	
	
	public int getSi_id() {
		return si_id;
	}
	public void setSi_id(int si_id) {
		this.si_id = si_id;
	}
	public String getSi_name() {
		return si_name;
	}
	public void setSi_name(String si_name) {
		this.si_name = si_name;
	}
	public String getSi_logo() {
		return si_logo;
	}
	public void setSi_logo(String si_logo) {
		this.si_logo = si_logo;
	}
	public String getSi_description() {
		return si_description;
	}
	public void setSi_description(String si_description) {
		this.si_description = si_description;
	}
	public String getSi_mission() {
		return si_mission;
	}
	public void setSi_mission(String si_mission) {
		this.si_mission = si_mission;
	}
	public String getSi_address() {
		return si_address;
	}
	public void setSi_address(String si_address) {
		this.si_address = si_address;
	}
	public int getSi_boys_hostel() {
		return si_boys_hostel;
	}
	public void setSi_boys_hostel(int si_boys_hostel) {
		this.si_boys_hostel = si_boys_hostel;
	}
	public int getSi_girls_hostel() {
		return si_girls_hostel;
	}
	public void setSi_girls_hostel(int si_girls_hostel) {
		this.si_girls_hostel = si_girls_hostel;
	}
	public long getSi_primary_phone() {
		return si_primary_phone;
	}
	public void setSi_primary_phone(long si_primary_phone) {
		this.si_primary_phone = si_primary_phone;
	}
	public long getSi_secondary_phone() {
		return si_secondary_phone;
	}
	public void setSi_secondary_phone(long si_secondary_phone) {
		this.si_secondary_phone = si_secondary_phone;
	}
	public String getSi_email() {
		return si_email;
	}
	public void setSi_email(String si_email) {
		this.si_email = si_email;
	}
	public long getSi_fax() {
		return si_fax;
	}
	public void setSi_fax(long si_fax) {
		this.si_fax = si_fax;
	}
	public String getSi_website() {
		return si_website;
	}
	public void setSi_website(String si_website) {
		this.si_website = si_website;
	}
	public int getSi_is_active() {
		return si_is_active;
	}
	public void setSi_is_active(int si_is_active) {
		this.si_is_active = si_is_active;
	}
	public City_Tb getSi_city() {
		return si_city;
	}
	public void setSi_city(City_Tb si_city) {
		this.si_city = si_city;
	}
	public School_Board_Tb getSi_board() {
		return si_board;
	}
	public void setSi_board(School_Board_Tb si_board) {
		this.si_board = si_board;
	}
	
	
}
