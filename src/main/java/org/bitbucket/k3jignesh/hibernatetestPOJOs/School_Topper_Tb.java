package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Topper_Tb {
	
	private int st_id;
	private int st_year;
	private String st_score;
		
		private School_Student_Tb st_student_id;
		private Class_Tb st_student_class;
		private Subject_Stream_Tb st_student_stream;
	
	
	public int getSt_id() {
		return st_id;
	}
	public void setSt_id(int st_id) {
		this.st_id = st_id;
	}
	public int getSt_year() {
		return st_year;
	}
	public void setSt_year(int st_year) {
		this.st_year = st_year;
	}
	public String getSt_score() {
		return st_score;
	}
	public void setSt_score(String st_score) {
		this.st_score = st_score;
	}
	public School_Student_Tb getSt_student_id() {
		return st_student_id;
	}
	public void setSt_student_id(School_Student_Tb st_student_id) {
		this.st_student_id = st_student_id;
	}
	public Class_Tb getSt_student_class() {
		return st_student_class;
	}
	public void setSt_student_class(Class_Tb st_student_class) {
		this.st_student_class = st_student_class;
	}
	public Subject_Stream_Tb getSt_student_stream() {
		return st_student_stream;
	}
	public void setSt_student_stream(Subject_Stream_Tb st_student_stream) {
		this.st_student_stream = st_student_stream;
	}
	
	
	
}
