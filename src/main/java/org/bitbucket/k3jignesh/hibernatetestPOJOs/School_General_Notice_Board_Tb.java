package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_General_Notice_Board_Tb {
	
	private int sgnb_id;
	private String sgnb_notice_entry;
	private String sgnb_uploaded_at;
	private int sgnb_have_attachment;
	private School_Information_Tb sgnb_school_id;
	
	
	public int getSgnb_id() {
		return sgnb_id;
	}
	public void setSgnb_id(int sgnb_id) {
		this.sgnb_id = sgnb_id;
	}
	public String getSgnb_notice_entry() {
		return sgnb_notice_entry;
	}
	public void setSgnb_notice_entry(String sgnb_notice_entry) {
		this.sgnb_notice_entry = sgnb_notice_entry;
	}
	public String getSgnb_uploaded_at() {
		return sgnb_uploaded_at;
	}
	public void setSgnb_uploaded_at(String sgnb_uploaded_at) {
		this.sgnb_uploaded_at = sgnb_uploaded_at;
	}
	public int getSgnb_have_attachment() {
		return sgnb_have_attachment;
	}
	public void setSgnb_have_attachment(int sgnb_have_attachment) {
		this.sgnb_have_attachment = sgnb_have_attachment;
	}
	public School_Information_Tb getSgnb_school_id() {
		return sgnb_school_id;
	}
	public void setSgnb_school_id(School_Information_Tb sgnb_school_id) {
		this.sgnb_school_id = sgnb_school_id;
	}
	
	

}
