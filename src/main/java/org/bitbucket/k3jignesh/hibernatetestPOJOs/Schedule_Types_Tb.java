package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class Schedule_Types_Tb {
	
	private int sst_id;
	private String sst_title;
	
	
	public int getSst_id() {
		return sst_id;
	}
	public void setSst_id(int sst_id) {
		this.sst_id = sst_id;
	}
	public String getSst_title() {
		return sst_title;
	}
	public void setSst_title(String sst_title) {
		this.sst_title = sst_title;
	}
	
	

}
