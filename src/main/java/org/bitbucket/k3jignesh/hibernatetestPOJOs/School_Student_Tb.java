package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Student_Tb {
	
	private int ss_id;
	private String ss_name;
	private String ss_image;
		
		private School_Information_Tb ss_school_id;
		private Class_Tb ss_current_class;
		private Subject_Stream_Tb subs_id;
	
	public Subject_Stream_Tb getSubs_id() {
			return subs_id;
		}
		public void setSubs_id(Subject_Stream_Tb subs_id) {
			this.subs_id = subs_id;
		}
	public int getSs_id() {
		return ss_id;
	}
	public void setSs_id(int ss_id) {
		this.ss_id = ss_id;
	}
	public String getSs_name() {
		return ss_name;
	}
	public void setSs_name(String ss_name) {
		this.ss_name = ss_name;
	}
	public String getSs_image() {
		return ss_image;
	}
	public void setSs_image(String ss_image) {
		this.ss_image = ss_image;
	}
	public School_Information_Tb getSs_school_id() {
		return ss_school_id;
	}
	public void setSs_school_id(School_Information_Tb ss_school_id) {
		this.ss_school_id = ss_school_id;
	}
	public Class_Tb getSs_current_class() {
		return ss_current_class;
	}
	public void setSs_current_class(Class_Tb ss_current_class) {
		this.ss_current_class = ss_current_class;
	}
	
	

}
