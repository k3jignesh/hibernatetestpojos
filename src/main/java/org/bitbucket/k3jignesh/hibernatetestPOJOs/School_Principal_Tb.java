package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Principal_Tb {

	private int sp_id;
	private String sp_name;
	private String sp_photo;
	private String sp_statement;
	private String sp_qualification;
	
	
		private School_Information_Tb sp_school_id;
		private School_User_Tb sp_user_id;
		
		
		
		public String getSp_qualification() {
			return sp_qualification;
		}
		public void setSp_qualification(String sp_qualification) {
			this.sp_qualification = sp_qualification;
		}
		public int getSp_id() {
			return sp_id;
		}
		public void setSp_id(int sp_id) {
			this.sp_id = sp_id;
		}
		public String getSp_name() {
			return sp_name;
		}
		public void setSp_name(String sp_name) {
			this.sp_name = sp_name;
		}
		public String getSp_photo() {
			return sp_photo;
		}
		public void setSp_photo(String sp_photo) {
			this.sp_photo = sp_photo;
		}
		public String getSp_statement() {
			return sp_statement;
		}
		public void setSp_statement(String sp_statement) {
			this.sp_statement = sp_statement;
		}
		public School_Information_Tb getSp_school_id() {
			return sp_school_id;
		}
		public void setSp_school_id(School_Information_Tb sp_school_id) {
			this.sp_school_id = sp_school_id;
		}
		public School_User_Tb getSp_user_id() {
			return sp_user_id;
		}
		public void setSp_user_id(School_User_Tb sp_user_id) {
			this.sp_user_id = sp_user_id;
		}
	
	
}
