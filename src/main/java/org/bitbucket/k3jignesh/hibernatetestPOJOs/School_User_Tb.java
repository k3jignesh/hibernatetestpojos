package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_User_Tb {

	private int su_id;
	private String su_username;
	private String su_password;
	
		private String su_email;
		private School_User_Type_tb su_type_id;
	
	
	public int getSu_id() {
		return su_id;
	}
	public void setSu_id(int su_id) {
		this.su_id = su_id;
	}
	public String getSu_username() {
		return su_username;
	}
	public void setSu_username(String su_username) {
		this.su_username = su_username;
	}
	public String getSu_password() {
		return su_password;
	}
	public void setSu_password(String su_password) {
		this.su_password = su_password;
	}
	public String getSu_email() {
		return su_email;
	}
	public void setSu_email(String su_email) {
		this.su_email = su_email;
	}
	public School_User_Type_tb getSu_type_id() {
		return su_type_id;
	}
	public void setSu_type_id(School_User_Type_tb su_type_id) {
		this.su_type_id = su_type_id;
	}
	
	
	
	
}
