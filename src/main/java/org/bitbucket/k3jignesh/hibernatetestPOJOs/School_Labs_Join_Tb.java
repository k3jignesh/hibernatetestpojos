package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Labs_Join_Tb {

	private int slj_id;
	private int slj_lab_count;
	
		private Lab_Types_Tb slj_lab_type_id;
		private School_Information_Tb slj_school_id;
	
	
	public int getSlj_id() {
		return slj_id;
	}
	public void setSlj_id(int slj_id) {
		this.slj_id = slj_id;
	}
	public int getSlj_lab_count() {
		return slj_lab_count;
	}
	public void setSlj_lab_count(int slj_lab_count) {
		this.slj_lab_count = slj_lab_count;
	}
	public Lab_Types_Tb getSlj_lab_type_id() {
		return slj_lab_type_id;
	}
	public void setSlj_lab_type_id(Lab_Types_Tb slj_lab_type_id) {
		this.slj_lab_type_id = slj_lab_type_id;
	}
	public School_Information_Tb getSlj_school_id() {
		return slj_school_id;
	}
	public void setSlj_school_id(School_Information_Tb slj_school_id) {
		this.slj_school_id = slj_school_id;
	}
	
	
	
}
