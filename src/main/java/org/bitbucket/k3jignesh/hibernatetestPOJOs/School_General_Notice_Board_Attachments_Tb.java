package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_General_Notice_Board_Attachments_Tb {
	
	private int sgnba_id;
	private String sgnba_file;
	
	private School_General_Notice_Board_Tb sgnba_sgnb_id;
	
	
	public int getSgnba_id() {
		return sgnba_id;
	}
	public void setSgnba_id(int sgnba_id) {
		this.sgnba_id = sgnba_id;
	}
	public String getSgnba_file() {
		return sgnba_file;
	}
	public void setSgnba_file(String sgnba_file) {
		this.sgnba_file = sgnba_file;
	}
	public School_General_Notice_Board_Tb getSgnba_sgnb_id() {
		return sgnba_sgnb_id;
	}
	public void setSgnba_sgnb_id(School_General_Notice_Board_Tb sgnba_sgnb_id) {
		this.sgnba_sgnb_id = sgnba_sgnb_id;
	}
	
	

}
