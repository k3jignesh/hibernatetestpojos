package org.bitbucket.k3jignesh.hibernatetestPOJOs;

public class School_Medium_Tb {

	private int sm_id;
	private String sm_language;
	
	
	public int getSm_id() {
		return sm_id;
	}
	public void setSm_id(int sm_id) {
		this.sm_id = sm_id;
	}
	public String getSm_language() {
		return sm_language;
	}
	public void setSm_language(String sm_language) {
		this.sm_language = sm_language;
	}
	
	
	
}
